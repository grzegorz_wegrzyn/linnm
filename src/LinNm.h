#ifndef LINNM_H
#define LINNM_H

#include "ComStackTypes.h"
#include "Nm.h"
#include "StandardTypes.h"
#include "LinNm_SchM.h"
#include "Det.h"


#define LINNM_VENDOR_ID 20
#define LINNM_MODULE_ID 50
#define LINNM_MAJOR_VERSION 1
#define LINNM_MINOR_VERSION 0
#define LINNM_PATCH_VERSION 20


typedef struct {
	EcucBooleanParamDef LinNmDevErrorDetect;
	EcucBooleanParamDef LinNmVersionInfoApi;
	EcucBooleanParamDef LinNmBusSynchronizationEnabled;
	EcucBooleanParamDef LinNmRemoteSleepIndicationEnabled;
	EcucBooleanParamDef LinNmUserDataEnabled;
	EcucBooleanParamDef LinNmStateChangeIndEnabled;
	EcucBooleanParamDef LinNmComControlEnabled;
	EcucBooleanParamDef LinNmPassiveModeEnabled;
	EcucBooleanParamDef LinNmSynchronizationPointEnabled;
	EcucBooleanParamDef LinNmCoorinatorSyncSupport;
} LinNm_ConfigType;

void LinNm_Init ( const LinNm_ConfigType* ConfigPtr ) ;
Std_ReturnType LinNm_PassiveStartUp ( NetworkHandleType nmChannelHandle );
Std_ReturnType LinNm_NetworkRequest ( NetworkHandleType nmChannelHandle );
Std_ReturnType LinNm_NetworkRelease ( NetworkHandleType nmChannelHandle );
void LinNm_GetVersionInfo ( Std_VersionInfoType* versioninfo );
Std_ReturnType LinNm_RequestBusSynchronization ( NetworkHandleType nmChannelHandle );
// Std_ReturnType LinNm_CheckRemoteSleepIndication ( NetworkHandleType nmChannelHandle, boolean* nmRemoteSleepIndPtr );
// Std_ReturnType LinNm_SetSleepReadyBit ( NetworkHandleType nmChannelHandle, boolean nmRemoteSleepReadyBit );
// Std_ReturnType LinNm_DisableCommunication ( NetworkHandleType NetworkHandle );
// Std_ReturnType LinNm_EnableCommunication ( NetworkHandleType NetworkHandle );

// Std_ReturnType LinNm_SetUserData ( NetworkHandleType NetworkHandle, const uint8* nmUserDataPtr );
// Std_ReturnType LinNm_GetUserData ( NetworkHandleType NetworkHandle, uint8* nmUserDataPtr );
// Std_ReturnType LinNm_GetPduData ( NetworkHandleType NetworkHandle, uint8* nmPduData );
// Std_ReturnType LinNm_RepeatMessageRequest ( NetworkHandleType NetworkHandle );
// Std_ReturnType LinNm_GetNodeIdentifier ( NetworkHandleType NetworkHandle, uint8* nmNodeIdPtr );
// Std_ReturnType LinNm_GetLocalNodeIdentifier ( NetworkHandleType NetworkHandle, uint8* nmNodeIdPtr );
// Std_ReturnType LinNm_GetState ( NetworkHandleType nmNetworkHandle, Nm_StateType* nmStatePtr, Nm_ModeType* nmModePtr );
// Std_ReturnType LinNm_Transmit ( PduIdType TxPduId, const PduInfoType* PduInfoPtr );
// Std_ReturnType LinNm_TxConfirmation ( PduIdType TxPduId, Std_ReturnType result );

// Call-back Notification:
// None

// Services IDs
#define LINNM_INIT_SERVICE_ID 0x00
#define LINNM_PASSIVESTARTUP_SERVICE_ID 			0x01
#define LINNM_NETWORKREQUEST_SERVICE_ID 			0x02
#define LINNM_NETWORKRELEASE_SERVICE_ID 			0x03
#define LINNM_GETVERSIONINFO_SERVICE_ID 			0xf1
#define LINNM_REQUESTBUSSYNCHRONIZATION_SERVICE_ID 	0xc0
#define LINNM_CHECKREMOTESLEEPINDICATION_SERVICE_ID 0xd0
#define LINNM_SETSLEEPREADYBIT_SERVICE_ID 			0x10
#define LINNM_DISABLECOMMUNICATION_SERVICE_ID 		0x04
#define LINNM_ENABLECOMMUNICATION_SERVICE_ID 		0x05

#define LINNM_SETUSERDATA_SERVICE_ID 			0x06
#define LINNM_GETUSERDATA_SERVICE_ID 			0x07
#define LINNM_GETPDUDATA_SERVICE_ID 			0x08
#define LINNM_REPEATMESSAGEREQUEST_SERVICE_ID 	0x09
#define LINNM_GETNODEIDENTIFIER_SERVICE_ID 		0x0a
#define LINNM_GETLOCALNODEIDENTIFIER_SERVICE_ID 0x0b
#define LINNM_GETSTATE_SERVICE_ID 				0x0e
#define LINNM_TRANSMIT_SERVICE_ID 				0x49
#define LINNM_TXCONFIRMATION_SERVICE_ID 		0x40

// Development error codes:
#define LINNM_E_UNINIT 				0x01
#define LINNM_E_INVALID_CHANNEL 	0x02
#define LINNM_E_PARAM_POINTER 		0x12
#define LINNM_E_INIT_FAILED 		0x13
#define LINNM_E_INVALID_PARAMETER 	0x14

#define MODULE_ID_LINNM 200

#endif