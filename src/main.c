#include <stdio.h>
#include "LinNm.h"

const LinNm_ConfigType cfg = {TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE};

uint8 TickNum;

#define tick \
	printf("Tick: %d\n", TickNum); \
	TickNum++; \
	LinNm_MainFunction();

int main() {
	TickNum = 0;
	//printf("Send NetworkRequest without initalization: %d\n",	LinNm_NetworkRequest(2));
	//LinNm_MainFunction();
	
	printf("LinNm initialization...\n");
	LinNm_Init(&cfg);
	
	printf("Run for one tick\n");
	tick
	
	printf("Send NetworkRequest: %d\n",	LinNm_NetworkRequest(2));
	tick
	
	printf("Send NetworkRelease: %d\n",LinNm_NetworkRelease(1));
	tick
	
	
	printf("Send PassiveStartUp: %d\n",LinNm_PassiveStartUp(4));
	tick
	
	for(uint8 i =0; i < 10; i++) {
		tick
	}
	
	
	printf("That's all. We can go home!\n");
	
}