#include "Nm.h"

void Nm_StateChangeNotification(NetworkHandleType nmChannel) {
	printf("Registered operational mode change at channel %d\n", nmChannel);
}

void Nm_BusSleepMode(NetworkHandleType nmChannel) {
	printf("Registered operational mode change to Bus Sleep Mode channel %d\n", nmChannel);
}

void Nm_NetworkMode(NetworkHandleType nmChannel) {
	printf("Registered operational mode change to Network Mode channel %d\n", nmChannel);
}