#include "LinNm.h"

#define CHANNEL_CNT_TODO 5

static LinNm_ConfigType* linnmConfigPtr = NULL;
static boolean PassiveStartup[CHANNEL_CNT_TODO];// = FALSE;
static uint8 TimeoutCounter[CHANNEL_CNT_TODO];
static boolean FlagChangeModeToNetwork[CHANNEL_CNT_TODO];
static boolean FlagChangeModeToBusSleep[CHANNEL_CNT_TODO];
static Nm_ModeType NetworkMode[CHANNEL_CNT_TODO];// = NM_MODE_BUS_SLEEP; // SWS_LinNm_00130
static Nm_StateType NetworkState[CHANNEL_CNT_TODO];// = NM_STATE_UNINIT; // SWS_LinNm_00130

// Development error macros: 
// SWS_LinNm_00034 SWS_LinNm_00037 SWS_LinNm_00038
#define VALIDATE(_exp,_sid,_err )                       \
        if( !(_exp) && linnmConfigPtr->LinNmDevErrorDetect == TRUE ) {                                 \
          Det_ReportError(MODULE_ID_LINNM,0,_sid,_err); \
          return;                                       \
        }

#define VALIDATE_RETURN(_exp,_sid,_err,_rv )            \
        if( !(_exp) && linnmConfigPtr->LinNmDevErrorDetect == TRUE ) {                                 \
          Det_ReportError(MODULE_ID_LINNM,0,_sid,_err); \
          return (_rv);                                 \
        }


// LinNm base functionality:
void LinNm_MainFunction() {
	
	
	
	for(uint8 i = 0; i < CHANNEL_CNT_TODO; i++){	// SWS_LinNm_00006
		if(PassiveStartup[i]) {
			TimeoutCounter[i]++;
			if(TimeoutCounter[i] > 5) {
				PassiveStartup[i] = FALSE;
				FlagChangeModeToBusSleep[i] = TRUE;
			}
		} else {
			TimeoutCounter[i] = 0;
		}
		if((FlagChangeModeToBusSleep[i] || FlagChangeModeToNetwork[i]) && linnmConfigPtr->LinNmStateChangeIndEnabled ) { // SWS_LinNm_00061
			Nm_StateChangeNotification(i);
		}
		if(FlagChangeModeToBusSleep[i]){		
			Nm_BusSleepMode(i);						// SWS_LinNm_00012
			FlagChangeModeToBusSleep[i] = FALSE;
			NetworkMode[i] = NM_MODE_BUS_SLEEP;		// SWS_LinNm_00005
			NetworkState[i] = NM_STATE_BUS_SLEEP;
			continue;
		} else if(FlagChangeModeToNetwork[i]){
			Nm_NetworkMode(i);						// SWS_LinNm_00008
			FlagChangeModeToNetwork[i] = FALSE;
			NetworkMode[i] = NM_MODE_NETWORK;		// SWS_LinNm_00005
			NetworkState[i] = NM_STATE_NORMAL_OPERATION;	
		}
	}
	//LinSM_TimerTick();
	Sleep(2);
}



// LinNm API:
// SWS_LinNm_00054
void LinNm_Init( const LinNm_ConfigType* ConfigPtr ) {	// SWS_LinNm_00102
	VALIDATE((ConfigPtr != NULL), LINNM_INIT_SERVICE_ID, LINNM_E_INVALID_CHANNEL) // SWS_LinNm_00038
	linnmConfigPtr = ConfigPtr;
	for(uint8 i = 0; i < CHANNEL_CNT_TODO; i++) {
		NetworkState[i] = NM_STATE_UNINIT;			// SWS_LinNm_00017
	}
	// Something more? .....
	for(uint8 i = 0; i < CHANNEL_CNT_TODO; i++) {
		PassiveStartup[i] = FALSE;
		TimeoutCounter[i] = 0;
		NetworkMode[i] = NM_MODE_BUS_SLEEP;			// SWS_LinNm_00020
		NetworkState[i] = NM_STATE_BUS_SLEEP;		// SWS_LinNm_00018 SWS_LinNm_00019
	}
}

// SWS_LinNm_00063
Std_ReturnType LinNm_PassiveStartUp ( NetworkHandleType nmChannelHandle ){	
	VALIDATE_RETURN((nmChannelHandle < CHANNEL_CNT_TODO), LINNM_PASSIVESTARTUP_SERVICE_ID, LINNM_E_INVALID_CHANNEL, E_NOT_OK)
	VALIDATE_RETURN((NetworkState[nmChannelHandle] != NM_STATE_UNINIT), LINNM_PASSIVESTARTUP_SERVICE_ID, LINNM_E_UNINIT, E_NOT_OK) // SWS_LinNm_00025 SWS_LinNm_00065
	
	if((NM_MODE_BUS_SLEEP == NetworkMode[nmChannelHandle]) && !FlagChangeModeToBusSleep[nmChannelHandle] && !FlagChangeModeToNetwork[nmChannelHandle]) { // SWS_LinNm_00064
		FlagChangeModeToNetwork[nmChannelHandle] = TRUE; // SWS_LinNm_00160
		PassiveStartup[nmChannelHandle] = TRUE;
		return E_OK;
	} else {
		return E_NOT_OK;	// SWS_LinNm_00022
	}
}

// SWS_LinNm_00055
Std_ReturnType LinNm_NetworkRequest ( NetworkHandleType nmChannelHandle ) {
	VALIDATE_RETURN((nmChannelHandle < CHANNEL_CNT_TODO), LINNM_NETWORKREQUEST_SERVICE_ID, LINNM_E_INVALID_CHANNEL, E_NOT_OK)
	VALIDATE_RETURN((NetworkState[nmChannelHandle] != NM_STATE_UNINIT), LINNM_NETWORKREQUEST_SERVICE_ID, LINNM_E_UNINIT, E_NOT_OK) // SWS_LinNm_00025 SWS_LinNm_00053
	
	if((NM_MODE_BUS_SLEEP == NetworkMode[nmChannelHandle]) && !FlagChangeModeToBusSleep[nmChannelHandle] && !FlagChangeModeToNetwork[nmChannelHandle]) {
		FlagChangeModeToNetwork[nmChannelHandle] = TRUE; // SWS_LinNm_00015
		PassiveStartup[nmChannelHandle] = FALSE;
		return E_OK;
	} else {
		return E_NOT_OK;	// SWS_LinNm_00156
	}
}

// SWS_LinNm_00056
Std_ReturnType LinNm_NetworkRelease ( NetworkHandleType nmChannelHandle ) {
	VALIDATE_RETURN((nmChannelHandle < CHANNEL_CNT_TODO), LINNM_NETWORKRELEASE_SERVICE_ID, LINNM_E_INVALID_CHANNEL, E_NOT_OK)
	VALIDATE_RETURN((NetworkState[nmChannelHandle] != NM_STATE_UNINIT), LINNM_NETWORKRELEASE_SERVICE_ID, LINNM_E_UNINIT, E_NOT_OK) // SWS_LinNm_00025 SWS_LinNm_00058
	
	if((NM_MODE_NETWORK == NetworkMode[nmChannelHandle]) && !FlagChangeModeToBusSleep[nmChannelHandle] && !FlagChangeModeToNetwork[nmChannelHandle]) {
		FlagChangeModeToBusSleep[nmChannelHandle] = TRUE; // SWS_LinNm_00016
		PassiveStartup[nmChannelHandle] = FALSE;
		return E_OK;
	} else {
		return E_NOT_OK;	// SWS_LinNm_00157
	}
}

// SWS_LinNm_00106
void LinNm_GetVersionInfo ( Std_VersionInfoType* versioninfo ) {
	VALIDATE((versioninfo != NULL), LINNM_GETVERSIONINFO_SERVICE_ID, LINNM_E_PARAM_POINTER) // SWS_LinNm_00163
	
	versioninfo->vendorID = LINNM_VENDOR_ID;
	versioninfo->moduleID = LINNM_MODULE_ID;
	versioninfo->sw_major_version = LINNM_MAJOR_VERSION;
	versioninfo->sw_minor_version = LINNM_MINOR_VERSION;
	versioninfo->sw_patch_version = LINNM_PATCH_VERSION;
}


Std_ReturnType LinNm_RequestBusSynchronization ( NetworkHandleType nmChannelHandle ) {
	VALIDATE_RETURN((nmChannelHandle < CHANNEL_CNT_TODO), LINNM_REQUESTBUSSYNCHRONIZATION_SERVICE_ID, LINNM_E_INVALID_CHANNEL, E_NOT_OK)
	VALIDATE_RETURN((NetworkState != NM_STATE_UNINIT), LINNM_REQUESTBUSSYNCHRONIZATION_SERVICE_ID, LINNM_E_UNINIT, E_NOT_OK)
	
	if(NM_MODE_BUS_SLEEP == NetworkMode[nmChannelHandle]) {
		return E_OK;					// SWS_LinNm_00042
	} else {
		return E_NOT_OK;
	}
}