#ifndef PLATFORMTYPES_H
#define PLATFORMTYPES_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

//typedef uint boolean;
//typedef bool boolean;
typedef boolean EcucBooleanParamDef;

#define FALSE 0
#define TRUE  1

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef int8_t sint8;
typedef int16_t sint16;
typedef int32_t sint32;
typedef int64_t sint64;


#endif