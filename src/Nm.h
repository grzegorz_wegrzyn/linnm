#ifndef NM_H
#define NM_H
#include <stdio.h>

#include "ComStackTypes.h"
#include "NmStack_types.h"


typedef struct {
	// TODO
} Nm_ConfigType;

void Nm_StateChangeNotification(NetworkHandleType nmChannel);
void Nm_BusSleepMode(NetworkHandleType nmChannel);
void Nm_NetworkMode(NetworkHandleType nmChannel);



#endif