#ifndef COMSTACKTYPES_H
#define COMSTACKTYPES_H

#include "StandardTypes.h"

typedef uint8 PduIdType;
typedef uint8 PduLengthType;

typedef struct {
	uint8* SduDataPtr;
	uint8* MetaDataPtr;
	PduLengthType SduLength;
} PduInfoType;

typedef uint8 PNCHandleType;

typedef enum {
	TP_STMIN=0x00,
	TP_BS,
	TP_BC
} TPParametesType;

typedef enum {
	BUFREQ_OK=0x00,
	BUFREQ_E_NOT_OK,
	BUFREQ_E_BUSY,
	BUFREQ_E_OVFL
} BufReq_ReturnType;

typedef enum {
	TP_DATACONF=0x00,
	TP_DATARETRY,
	TP_CONFPENDING
} TpDataStateType;

typedef struct {
	TpDataStateType TpDataState;
	PduLengthType TxTpDataCnt;
} RestryInfoType;

typedef uint8 NetworkHandleType;
typedef uint8 IcomConfigIdType;

typedef enum {
	ICOM_SWITCH_E_OK=0x00,
	ICOM_SWITCH_E_FAILED
} IcomSwitch_ErrorType;

#endif